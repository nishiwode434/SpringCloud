package com.wjs.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * SpringCloud class
 *
 * @author wujinsheng
 * @date 2017/11/10
 */
@Component
public class AccessFilter extends ZuulFilter {
    @Override
    public String filterType() {
        return "pre";
    }

    //数值越小优先级越高
    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext rc = RequestContext.getCurrentContext();
        HttpServletRequest request = rc.getRequest();
        Object o = request.getParameter("accessToken");
        if (null == o) {
            System.out.println("access token is null!!!");
            rc.setSendZuulResponse(false);
            rc.setResponseStatusCode(401);
            return null;
        }
        System.out.println("access token is ok!!!");
        try {
           throw new IllegalArgumentException("发生异常情况！");
        } catch (Exception e) {

        }
        return null;
    }
}

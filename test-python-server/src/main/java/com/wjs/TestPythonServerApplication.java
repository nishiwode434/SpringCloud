package com.wjs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.sidecar.EnableSidecar;

@EnableSidecar
@EnableEurekaClient
@SpringBootApplication
public class TestPythonServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestPythonServerApplication.class, args);
	}
}

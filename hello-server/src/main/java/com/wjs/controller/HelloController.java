package com.wjs.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * SpringCloud class
 *
 * @author wujinsheng
 * @date 2017/11/9
 */

@RestController
@RequestMapping("welcome/test")
public class HelloController {
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public ResponseEntity testHello(){
        return new ResponseEntity("欢迎进入", HttpStatus.OK);
    }
}
